redmine for Debian
==================

IMPORTANT: change the default password
-------------------------------------

On installation, Redmine is configured with a admin account with username
"admin" and password "admin". Just after installation, make sure you change the
default password: just login to the web interface with it, and Redmine will
redirect you the "change password" page.


Web server configuration - the easiest way
------------------------------------------

This package does NOT configure the web server automatically for you.

The easiest way to get Redmine up and running is using the Passenger
application server, integrated with the Apache webserver:

```
apt install apache2 libapache2-mod-passenger

cp /usr/share/doc/redmine/examples/apache2-passenger-host.conf \
   /etc/apache2/sites-available/redmine.conf

edit /etc/apache2/sites-available/redmine.conf
# change at least the ServerName directive, configure HTTPS etc

a2enmod passenger
a2ensite redmine.conf
service apache2 reload
```


Redmine documentation
---------------------

General documentation about Redmine can be found at
https://www.redmine.org/wiki/redmine/Guide


rake commands
-------------

Several Redmine management tasks require running rake commands; note that on
Debian they must be executed from inside the /usr/share/redmine directory, as
the www-data user. Example:

```
cd /usr/share/redmine
sudo -u www-data rake ...
```


Support for multiple instances
------------------------------

This Redmine package in Debian is designed to support running multiple Redmine
instances, each one with its separate database, file attachments, and secret
keys. The default instance is called "default".

A debconf facility is provided for configuring several redmine
instances. Use dpkg-reconfigure to define the instances identifiers:

```
dpkg-reconfigure redmine
```

To have any rake/rails commands operate on instances othen than "default", you
need to specify the instance in the `REDMINE_INSTANCE` environment variable.
For example, to run the IMAP email receiver for the "customer1" instance, use:

```
sudo -u www-data REDMINE_INSTANCE=customer1 RAILS_ENV=production \
    rake \
    redmine:email:receive_imap \
    host=imap.test.com \
    username=test@test.com password=xxxxxx
```

If `$REDMINE_INSTANCE` is omitted, Redmine will operate on the instance
"default".

Web server configuration - alternative ways
-------------------------------------------

There are several ways to serve a Rails application like Redmine. The easiest
way is using Passenger+Apache (see above). Some examples are available in
/usr/share/doc/redmine/examples.

Quick launch using WEBrick
--------------------------

To test redmine without installing a web server:


```
cd /usr/share/redmine/
sudo -u www-data REDMINE_INSTANCE=default rackup -E production
```

Then point your browser to http://localhost:9292/. See rackup(1) for more
options.

Note that running without sudoing to www-data will change permissions on some
files, e.g. log files. Don't do that.



email configuration
-------------------

Email settings are not automatically configured. You need to create
`/etc/redmine/<instancename>/configuration.yml` and add your email settings. If
you have a local SMTP server, the shortest configuration that will work is:

```
default:
  email_delivery:
    delivery_method: :smtp
    smtp_settings:
      address: "localhost"
      port: 25
      enable_starttls_auto: false
```

If you use an external SMTP server, or other setup, check
https://www.redmine.org/projects/redmine/wiki/EmailConfiguration for more
information.

If your email configuration contains sensitive information such as passowords,
you will want to protect it by changing its owner and permissions:

```
chown root:www-data /etc/redmine/<instancename>/configuration.yml
chmod 640 /etc/redmine/<instancename>/configuration.yml
```

Then restart redmine (service apache2 reload) and check if it's working by
sending a test email in Administration panel -> Settings -> Email notifications
-> Send a test email. Refer to the Redmine Guide for more information.


WARNING : delivery_method: :async_smtp
--------------------------------------

It has been reported [0] that this method does not work when used by a rake
task (e.g.  in a crontab). Simply use :smtp method in that case.

[0] https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=570402


 -- Ondřej Surý <ondrej@debian.org>, Tue, 31 May 2011 09:30:21 +0200
 -- Antonio Terceiro <terceiro@debian.org>  Sat, 26 Nov 2016 00:04:28 -0200

# vim: ft=markdown
